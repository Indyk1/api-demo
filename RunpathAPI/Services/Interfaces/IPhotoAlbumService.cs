﻿using RunpathAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RunpathAPI.Services.Interfaces
{
    public interface IPhotoAlbumService
    {
        Task<IEnumerable<PhotoAlbum>> GetAllPhotoAlbumsAsync();
        Task<IEnumerable<PhotoAlbum>> GetPhotoAlbumAsync(int userId);
    }
}

﻿using RunpathAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RunpathAPI.Services.Interfaces
{
    interface IAlbumService
    {
        Task<IEnumerable<Album>> GetAlbumsAsync();
    }
}

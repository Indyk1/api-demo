﻿using RunpathAPI.Models;
using RunpathAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RunpathAPI.Services.Concrete
{
    public class PhotoAlbumService : IPhotoAlbumService
    {
        public async Task<IEnumerable<PhotoAlbum>> GetAllPhotoAlbumsAsync()
        {
            PhotoService photoService = new PhotoService();
            AlbumService albumService = new AlbumService();
            IEnumerable<Photo> photos = await photoService.GetPhotosAsync();
            IEnumerable<Album> albums = await albumService.GetAlbumsAsync();

            List<PhotoAlbum> photoAlbums = new List<PhotoAlbum>();

            foreach (var album in albums)
            {
                PhotoAlbum photoAlbum = new PhotoAlbum() {
                    Id = album.Id,
                    UserId = album.UserId,
                    Title = album.Title,
                    Photos = photos.Where(x => x.AlbumId == album.Id)
                };
                photoAlbums.Add(photoAlbum);
            }

            return photoAlbums;
        }

        public async Task<IEnumerable<PhotoAlbum>> GetPhotoAlbumAsync(int userId)
        {
            IEnumerable<PhotoAlbum> photoAlbums = await GetAllPhotoAlbumsAsync();
            return photoAlbums.Where(x => x.UserId == userId);
        }
    }
}

﻿using Newtonsoft.Json;
using RunpathAPI.Models;
using RunpathAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RunpathAPI.Services.Concrete
{
    public class PhotoService : IPhotoService
    {
        public async Task<IEnumerable<Photo>> GetPhotosAsync()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync("http://jsonplaceholder.typicode.com/photos");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IEnumerable<Photo>>(responseBody);
        }
    }
}

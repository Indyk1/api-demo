﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RunpathAPI.Models;
using RunpathAPI.Services.Concrete;
using RunpathAPI.Services.Interfaces;

namespace RunpathAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PhotoAlbumController : ControllerBase
    {
        private readonly IPhotoAlbumService _service;

        public PhotoAlbumController(IPhotoAlbumService service)
        {
            _service = service;
        }

        // GET /PhotoAlbum
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                IEnumerable<PhotoAlbum> photoAlbums = await _service.GetAllPhotoAlbumsAsync();
                return Ok(photoAlbums);
            }
            catch (NullReferenceException ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET /PhotoAlbum/5
        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(int userId)
        {
            try
            {
                IEnumerable<PhotoAlbum> usersPhotoAlbums = await _service.GetPhotoAlbumAsync(userId);
                if (usersPhotoAlbums.Count() == 0)
                {
                    return NotFound();
                }
                return Ok(usersPhotoAlbums);
            }
            catch (NullReferenceException ex)
            {
                return NotFound(ex.Message);
            }
            
        }
    }
}

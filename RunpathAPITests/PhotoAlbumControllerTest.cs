using Microsoft.AspNetCore.Mvc;
using RunpathAPI.Controllers;
using RunpathAPI.Models;
using RunpathAPI.Services.Concrete;
using RunpathAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RunpathAPITests
{
    public class PhotoAlbumControllerTest
    {
        readonly PhotoAlbumController _controller;
        readonly IPhotoAlbumService _service;

        public PhotoAlbumControllerTest()
        {
            _service = new PhotoAlbumService();
            _controller = new PhotoAlbumController(_service);
        }

        [Fact]
        public async Task GetAll_WhenCalled_ReturnsOkResult()
        {
            // Act
            var actionResult = await _controller.Get() as OkObjectResult;

            // Assert
            Assert.Equal(200, actionResult.StatusCode);
        }

        [Fact]
        public async Task Get_WhenCalled_ReturnsOkResult()
        {
            // Act
            var actionResult = await _controller.Get(1) as OkObjectResult;

            // Assert
            Assert.Equal(200, actionResult.StatusCode);
        }

        [Fact]
        public async Task Get_WhenCalled_ReturnsCorrectUser()
        {
            //Arrange
            var expected = 1;

            // Act
            var actionResult = await _controller.Get(expected);

            // Assert
            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);

            var model = okObjectResult.Value as IEnumerable<PhotoAlbum>;
            Assert.NotNull(model);

            var actual = model.FirstOrDefault().UserId;
            Assert.Equal(expected, actual);
        }
    }
}
